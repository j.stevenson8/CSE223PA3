import java.util.Scanner;
import java.lang.String;

public class PA3
{
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args)
	{
		String file;
		try
		{
			file = args[0];
		}
		catch(Exception e)		
		{
			file = "20Q.txt";
		}
			
		Tree tree = new Tree(file);
		boolean game = tree.printNode();
		String input = "";

		while(game)
		{
			input = sc.nextLine();

			if(input.compareTo("yes") == 0)
			{
				tree.traverseYes();
			}
			else if(input.compareTo("no") == 0)
			{
				tree.traverseNo();
			}
			
			game = tree.printNode();
		}
		
		input = sc.nextLine();
		
		if(input.compareTo("yes") == 0)
		{
			System.out.println("Yay! I win!");
			return;
		}

		System.out.println("Darn!\nWhat were you thinking of?");
		String thing = sc.nextLine();
		System.out.println("Think of a question that has an answer of yes for " + tree.currNode.contents + " and no for " + thing);
		String question = sc.nextLine();
		tree.addNew(thing, question);
		tree.writeToFile();
		return;
	}
}

		
