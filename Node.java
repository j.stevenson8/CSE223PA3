public class Node
{
	String contents = "";
	boolean isQuestion = true;
	Node yes = null;
	Node no = null;

	public Node(String string)
	{
		contents = string;
		return;
	}

	public void addYes(Node newNode)
	{
		yes = newNode;
		return;
	}
	
	public void addNo(Node newNode)
	{
		no = newNode;
		return;
	}
}	
