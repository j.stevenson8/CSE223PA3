import java.io.PrintWriter;
import java.io.File;
import java.util.Scanner;

public class Tree
{
	Node root;
	Scanner sc;
	Node prevNode;
	Node currNode;
	String lastMove;
	PrintWriter pr;
	File f;

	public Tree(String file)
	{
		try
		{
			f = new File(file);
			sc = new Scanner(f);
		}
		catch(Exception e) 
		{
			//file does not exist
			//should be handled in main program
		}

		root = currNode = buildTree(root);
		return;
	}
	
	private Node buildTree(Node n)
	{
		if(!sc.hasNextLine())
		{
			return null;
		}

		String classifier = sc.nextLine();
		String contents = sc.nextLine();
		
		if(classifier.compareTo("A:") == 0)
		{
			n = new Node(contents);
			n.isQuestion = false;
			return n;
		}
		else
		{
			n = new Node(contents);
		}			
		
		n.yes = buildTree(n.yes);
		n.no = buildTree(n.no);
		return n;
	}

	public void traverseYes()
	{
		prevNode = currNode;
		currNode = currNode.yes;
		lastMove = "yes";
		return;
	}
	
	public void traverseNo()
	{
		prevNode = currNode;
		currNode = currNode.no;
		lastMove = "no";
		return;
	}

	public boolean printNode()
	{
		if(!currNode.isQuestion)
		{
			System.out.println("Are you thinking of " + currNode.contents + "?");
			return false;
		}
		
		System.out.println(currNode.contents);
		return true;
	}

	public void addNew(String thing, String question)
	{
		Node q = new Node(question);
		Node a = new Node(thing);
	
		a.isQuestion = false;
		q.no = a;
		q.yes = currNode;
		
		if(lastMove.equals("yes"))
		{
			prevNode.yes = q;
			return;
		}

		prevNode.no = q;
		return;
	}

	public void writeToFile()
	{
		f.delete();
		try
		{
			f.createNewFile();
		}
		catch(Exception e)
		{
			//file is unable to be created 
			//should not happen
			return;
		}

		f.setWritable(true);

		try
		{
			pr = new PrintWriter(f);
		}
		catch(Exception e)
		{
			//file does not exist
			//should be handled in main program
			return;
		}
		write(root);
		return;		
	}
		
	public void write(Node n)
	{
		if(n.isQuestion)
		{
			pr.write("Q:\n");
			pr.write(n.contents + "\n");
			pr.flush();
			write(n.yes);
			write(n.no);
			return;
		}
		
		pr.write("A:\n");
		pr.write(n.contents + "\n");
		pr.flush();
		return;
	}
}	
